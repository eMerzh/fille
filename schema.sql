DROP TABLE IF EXISTS item;
CREATE TABLE item (
  id integer PRIMARY KEY autoincrement,
  title text NOT NULL,
  description text NOT NULL DEFAULT '',
  url text NOT NULL,
  picture text NOT NULL,
  reserved_by text NOT NULL DEFAULT '',
  reserved_by_mail text NOT NULL DEFAULT '',
  reserved_word text NOT NULL DEFAULT '',
  reserved_at integer,
  is_visible integer NOT NULL DEFAULT 1,
);
INSERT INTO item (title, url, picture)
VALUES('bro', 'http://hello.com', ''),
VALUES('Coucou', 'http://google.com', '');