$(function() {
  // Oops
  $(".open-trigger").bind("click", function(event) {
    $("#app").addClass("open");
  });

  $("nav a").click(function(e) {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top
      },
      500
    );
  });

  // Put the gallery thumbs into a modal on click
  $("a.thumbnail").click(function(e) {
    e.preventDefault();
    var $modal = $("#galleryModal");
    $modal.find(".modal-body").empty();
    $(
      $(this)
        .parents("div")
        .html()
    ).appendTo("#galleryModal .modal-body");
    $modal
      .find(".modal-body")
      .find("a")
      .bind("click", function(e) {
        e.preventDefault();
      });
    $("#galleryModal").modal({ show: true });
  });

  // Toggle the gift type details
  $("[name=gift_type]").change(changeGiftType);

  function changeGiftType() {
    var gift_type = $("[name=gift_type]:checked").val();
    $(".more_info_click_and_go").toggle(gift_type == "click_and_go");
    $(".more_info_marche").toggle(gift_type == "marche");
  }

  // Fill the form with hidden values when click on shop button
  $("#giftModal").on("show.bs.modal", function(e) {
    // Hide success Message
    $(".modal-success").hide();
    $(".modal-form").show();

    var $sourceButton = $(e.relatedTarget);
    // Assign values to form
    $("#gift").val($sourceButton.data("giftid"));
    $("#giftModal .price").text($sourceButton.data("price"));
    $("#giftModal .more_info_marche").text($sourceButton.data("buy-desc"));

    // Limit choices of gift type
    if ($sourceButton.data("isonlygo")) {
      $("#gift_type_click_and_go").prop("checked", true);
      $(".marche_type").hide();
    } else {
      $(".marche_type").show();
    }
    changeGiftType();
  });

  // Form submit
  $("#gift_form").submit(function(e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
      type: form.attr("method"),
      url: form.attr("action"),
      data: form.serialize(),
      success: function(data) {
        if (data.success) {
          var giftId = $("#gift").val();
          $('[data-gift-id="' + giftId + '"]').addClass("reserved");
          $('[data-gift-id="' + giftId + '"]')
            .find("button")
            .prop("disabled", true);
          $(".modal-success").show();
          $(".modal-form").hide();
          //location.reload(true);
        } else {
          $("#modalError").show();
        }
      }
    });
  });
});
