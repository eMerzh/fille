#!/usr/bin/python
# -*- coding: UTF8 -*-
import os
import os.path as op
import sqlite3
import time
import urllib

from flask import Flask, jsonify, render_template, request, current_app
from flask_admin import Admin, AdminIndexView, expose
from flask_admin.contrib.fileadmin import FileAdmin
from flask_admin.contrib.sqla import ModelView
from flask_httpauth import HTTPBasicAuth
from flask_sqlalchemy import SQLAlchemy
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from sqlalchemy import event

app = Flask(__name__, static_folder="static", static_url_path="")
app.config.from_object(__name__)

# Load default config and override config from an environment variable
app.config.update(
    dict(
        DEBUG=True,
        SECRET_KEY="Fille Dev KeyboardInterrupt",
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        SQLALCHEMY_DATABASE_URI="sqlite:///" + os.path.join(app.root_path, "flaskr.db"),
        FROM_EMAIL="brice@bmaron.net",
        TO_EMAIL="bmaron@gmail.com",
        SENDGRID_API_KEY="",
    )
)

app.config.from_envvar("FLASKR_SETTINGS", silent=True)

db = SQLAlchemy(app)

USERS = {
    "nouk": "kev12",
    "power": "ranger",
}

auth = HTTPBasicAuth()


class GiftCategory(db.Model):
    __tablename__ = "gift_category"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    order = db.Column(db.Integer)
    gifts = db.relationship("Gift", backref="category", lazy="dynamic")

    def __repr__(self):
        return self.name

    @property
    def has_gift_left(self):
        return any([item for item in self.gifts if item.is_visible])


class Gift(db.Model):
    __tablename__ = "gift"

    id = db.Column(db.Integer, primary_key=True)
    category_id = db.Column(db.Integer, db.ForeignKey("gift_category.id"))
    title = db.Column(db.String())
    description = db.Column(db.String())
    buy_description = db.Column(db.String())
    is_only_go = db.Column(db.Boolean(), default=True)
    price = db.Column(db.String(), default=False)

    url = db.Column(db.String())
    picture = db.Column(db.String())
    reserved_by = db.Column(db.String())
    reserved_by_mail = db.Column(db.String())
    reserved_word = db.Column(db.String())
    reserved_type = db.Column(db.String())
    reserved_at = db.Column(db.Integer())
    is_visible = db.Column(db.Boolean(), default=True)

    @property
    def is_only_go_value(self):
        return "1" if self.is_only_go else ""

    def __repr__(self):
        return self.title


def add_file(mapper, connection, target):
    if target.picture and not target.url:
        try:
            # print target.picture
            url = urllib.request.urlopen(target.picture).geturl()
            filename = url.split("/")[-1]
            target.url = filename
            urllib.request.urlretrieve(target.picture, "static/gifts/" + filename)
            # os.remove(op.join(file_path, target.path))
        except OSError:
            # Don't care if was not deleted because it does not exist
            pass


event.listen(Gift, "before_insert", add_file)
event.listen(Gift, "before_update", add_file)

# Create customized index view class that handles login & registration
class MyAdminIndexView(AdminIndexView):
    @auth.login_required
    @expose("/")
    def index(self):
        return super(MyAdminIndexView, self).index()


@auth.get_password
def get_pw(username):
    if username in USERS:
        return USERS.get(username)
    return None


admin = Admin(app, "Admin", index_view=MyAdminIndexView(), template_mode="bootstrap3")


class CommonModelView(ModelView):
    form_args = {
        "picture": {"label": "Picture URL"},
        "url": {"label": "Image URL (autofilled)"},
    }
    form_excluded_columns = [
        "reserved_by",
        "reserved_by_mail",
        "reserved_word",
        "reserved_type",
        "reserved_at",
    ]

    def is_accessible(self):
        try:
            if auth.username():
                return True
        except:
            return False


class GiftFullEdit(CommonModelView):
    form_args = {
        "picture": {"label": "Picture URL"},
        "url": {"label": "Image URL (autofilled)"},
    }
    form_excluded_columns = None

    def is_accessible(self):
        try:
            print(auth.username())
            if auth.username() == "power":
                return True
        except:
            return False


@app.route("/")
def index():
    categories = GiftCategory.query

    return render_template("index.html", categories=categories)


@app.route("/update_status", methods=["POST", "GET"])
def update_status():
    if "gift" in request.form.keys():
        gift = Gift.query.filter(Gift.id == request.form["gift"]).one()

        gift.reserved_by = request.form["name"]
        gift.reserved_by_mail = request.form["mail"]
        gift.reserved_word = request.form["reserved_word"]
        gift.reserved_type = request.form["gift_type"]
        gift.reserved_at = int(time.time())

        db.session.add(gift)
        db.session.commit()

        message = Mail(
            from_email=current_app.config["FROM_EMAIL"],
            to_emails=current_app.config["TO_EMAIL"],
            subject=f"Réservation par {gift.reserved_by}",
            html_content=f"Yey {gift.reserved_by} a choisi {gift.title}...<br /> le message est : <br> <pre>{gift.reserved_word}</pre>",
        )
        try:
            sg = SendGridAPIClient(current_app.config["SENDGRID_API_KEY"])
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(str(e))

        return jsonify(success=True)
    else:
        return jsonify(error=True)


admin.add_view(CommonModelView(GiftCategory, db.session))
admin.add_view(CommonModelView(Gift, db.session))
admin.add_view(GiftFullEdit(Gift, db.session, endpoint="fulledit", name="Full Edit"))
admin.index_view = MyAdminIndexView()


if __name__ == "__main__":
    app.run()
