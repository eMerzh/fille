#!/usr/bin/env python

"""
Passenger WSGI script for hosting on Dreamhost. Assumes a virtualenv
configured at ~/env/[appname].
"""

import sys
import os

INTERP = os.path.expanduser("~/venv_fille/bin/python")
if sys.executable != INTERP:
    os.execl(INTERP, INTERP, *sys.argv)

sys.path.append(os.getcwd())
# from hello import app as application
from app import app as application
