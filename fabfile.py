from fabric.api import *
from fabric.contrib.project import rsync_project

# the user to use for the remote commands
env.user = "pysite"
# the servers where the commands are executed
env.hosts = ["fille.bmaron.net"]
env.path = "/home/pysite/fille.bmaron.net/"


def sync():
    """
    Synchronize project with webserver
    """

    rsync_project(
        env.path,
        exclude=[
            ".git",
            "*.pyc",
            "venv",
            "venv2",
            ".DS_Store",
            "node_modules",
            "flaskr.db",
        ],
        local_dir=".",
    )


def restart():
    run("touch " + env.path + "tmp/restart.txt")


def deploy():
    execute(sync)
    execute(restart)


def getDb():
    local("cp flaskr.db flaskr.db.bkp")
    get(env.path + "flaskr.db", ".")


# def getBackup():
#    rsync_project(env.path, exclude=['*.pyc', 'venv', '.DS_Store', '.git'], local_dir='.')

# . bin/activate
# pip install -r requirements.txt
